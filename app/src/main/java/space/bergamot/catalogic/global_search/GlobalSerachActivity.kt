package space.bergamot.catalogic.global_search

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import io.realm.Realm
import io.realm.Sort
import kotlinx.android.synthetic.main.activity_global_serach.*
import space.bergamot.catalogic.R
import space.bergamot.catalogic.recycler_add_items_groups.Dataitem
import space.bergamot.catalogic.recycler_add_items_groups.MyAddItemRecyclerAdapter
import space.bergamot.catalogic.recycler_main.Group
import space.bergamot.catalogic.recycler_main.ItemsRecyclerAdapter
import java.io.DataInput

class GlobalSerachActivity : AppCompatActivity() {
    private lateinit var realm: Realm
    private lateinit var myGroupsAdapter: GSGroupsRecyclerAdapter
    private lateinit var itemsRecyclerAdapter: GSItemsRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_global_serach)
        realm = Realm.getDefaultInstance()

        // Установка адаптеров со списком групп и менеджера
        myGroupsAdapter = GSGroupsRecyclerAdapter(realm.where(Group::class.java).findAll(),this)
        global_search_groups_recycler.adapter = myGroupsAdapter
        global_search_groups_recycler.layoutManager = LinearLayoutManager(this)

        itemsRecyclerAdapter = GSItemsRecyclerAdapter(realm.where(Dataitem::class.java).findAll())
        global_search_items_recycler.adapter = itemsRecyclerAdapter
        global_search_items_recycler.layoutManager = LinearLayoutManager(this)
    }

    fun onItemsSelected(){
        itemsRecyclerAdapter.setDataitems(
                realm.where(Dataitem::class.java)
                        .contains("groups",myGroupsAdapter.getChecked())
                        .sort("date", Sort.DESCENDING)
                        .findAll()
        )
        itemsRecyclerAdapter.notifyDataSetChanged()
    }

    override fun onDestroy() {
        realm.close()
        super.onDestroy()
    }
}
