package space.bergamot.catalogic.global_search;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.List;

import space.bergamot.catalogic.AddItemActivity;
import space.bergamot.catalogic.R;
import space.bergamot.catalogic.recycler_add_items_groups.Dataitem;

public class GSItemsRecyclerAdapter extends RecyclerView.Adapter<GSItemsRecyclerAdapter.GroupViewHolder> {

    private WeakReference<Context> context;
    private List<Dataitem> dataitems;
    public void setDataitems(List<Dataitem> dataitems) {
        this.dataitems = dataitems;
    }

    public GSItemsRecyclerAdapter(List<Dataitem> dataitems) {this.dataitems = dataitems;}

    @NonNull
    @Override
    public GroupViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        context = new WeakReference<>(parent.getContext());
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.global_card_item, parent, false);
        return new GroupViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupViewHolder holder, int position)
    {
        holder.bind(dataitems.get(position));
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(context.get(), AddItemActivity.class);
//                intent.putExtra("itemID",dataitems.get(position).getPrimKey());
//                context.get().startActivity(intent);
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return dataitems.size();
    }


    class GroupViewHolder extends RecyclerView.ViewHolder
    {
        ImageView imageView;
        TextView textViewName;
        TextView textViewDesc;

        GroupViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.gs_card_item__image);
            textViewName = itemView.findViewById(R.id.gs_card_item__text);
            textViewDesc = itemView.findViewById(R.id.gs_card_item__desc);
        }

        void bind(Dataitem dataitem) {
            if (!TextUtils.isEmpty(dataitem.getPhoto())) {
                Picasso.get()
                        .load(dataitem.getPhoto())
                        .fit()
                        .centerCrop()
                        .into(imageView);
            }

            textViewName.setText(dataitem.getName());
            textViewDesc.setText(dataitem.getDescription());
        }
    }
}
