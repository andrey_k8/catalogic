package space.bergamot.catalogic;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import java.io.File;
import java.util.List;
import java.util.Objects;

import io.realm.Realm;
import space.bergamot.catalogic.global_search.GlobalSerachActivity;
import space.bergamot.catalogic.recycler_main.Group;
import space.bergamot.catalogic.recycler_main.SectionsPagerAdapter;

public class MainActivity extends AppCompatActivity {

    private Realm realm;
    private List<Group> groupList;
    private SectionsPagerAdapter sectionsPagerAdapter;
    private TabLayout tabs;
    private ViewPager viewPager;
    private String searchReq = "";
    private File xlsFileLocation;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        Objects.requireNonNull(toolbar.getOverflowIcon()).setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
        setSupportActionBar(toolbar);
        realm = Realm.getDefaultInstance();
        groupList = realm.where(Group.class).findAll();

        SharedPreferences mSettings = getSharedPreferences("mSettings", Context.MODE_PRIVATE);

        if (groupList.size() == 0 && !mSettings.contains("firstRun")) {
            realm.executeTransaction(realm -> realm.insertOrUpdate(new Group("Main", "#7CB342")));

            SharedPreferences.Editor editor = mSettings.edit();
            editor.putBoolean("firstRun", false);
            editor.apply();
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        {
            fab.setOnClickListener(view -> {
                if (groupList.isEmpty())
                    Snackbar.make(view, getString(R.string.error_no_group_existis), Snackbar.LENGTH_SHORT)
                            .setAction("Action", null).show();
                else {
                    Intent intent = new Intent(MainActivity.this, AddItemActivity.class);
                    intent.putExtra("groupID", groupList.get(tabs.getSelectedTabPosition()).getPrimKey());
                    startActivity(intent);
                }
            });
        }

        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), groupList);
        {
            viewPager = findViewById(R.id.view_pager);
            viewPager.setAdapter(sectionsPagerAdapter);
            tabs = findViewById(R.id.tabs);
            tabs.setTabTextColors(
                    getResources().getColor(R.color.colorBackground)
                    ,
                    getResources().getColor(R.color.colorBlack)
            );
            tabs.setTabRippleColorResource(R.color.colorBlack);
            tabs.setTabMode(TabLayout.MODE_SCROLLABLE);
            tabs.setupWithViewPager(viewPager);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        if (groupList.isEmpty()) {
            menu.getItem(0).setVisible(false);
            menu.getItem(2).setVisible(false);
            menu.getItem(3).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_add_new_group_activity) {
            Intent intent = new Intent(MainActivity.this, AddGroupActivity.class);
            startActivity(intent);
            return true;
        }

        if (id == R.id.action_edit_group) {
            Intent intent = new Intent(this, AddGroupActivity.class);
            intent .putExtra("primKey", groupList.get(tabs.getSelectedTabPosition()).getPrimKey());
            startActivity(intent);
            return true;
        }

        if (id == R.id.main_search) {
            handleSearch(item);
            return true;
        }

        if (id == R.id.global_search){
            startActivity(new Intent(this, GlobalSerachActivity.class));
        }

        if (id == R.id.export) {
            if (xlsFileLocation != null) {
                File dir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
                new File(dir, xlsFileLocation.getName()).delete();
            }
            xlsFileLocation = ExportToExcell.getExportFile(this);
            Uri path = Uri.fromFile(xlsFileLocation);
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent .setType("vnd.android.cursor.dir/email");
            emailIntent .putExtra(Intent.EXTRA_STREAM, path);
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            startActivity(Intent.createChooser(emailIntent , getString(R.string.export)));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy()
    {
        realm.close();
        if (xlsFileLocation != null) {
            File dir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
            new File(dir, xlsFileLocation.getName()).delete();
        }
        super.onDestroy();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        groupList = realm.where(Group.class).findAll();
        tabs.invalidate();
        sectionsPagerAdapter.setGroups(groupList);
        sectionsPagerAdapter.notifyDataSetChanged();
        invalidateOptionsMenu();
    }

    private void handleSearch(MenuItem menuItem)
    {
        SearchView searchView = (SearchView) menuItem.getActionView();
        EditText searchEditText = searchView.findViewById(R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.colorBlack));
        searchEditText.setHintTextColor(getResources().getColor(R.color.colorBlackTransparent));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchReq = (newText);
                sectionsPagerAdapter.notifyDataSetChanged();
                return true;
            }
        });
    }

    public Realm getRealm() {
        return realm;
    }

    public String getSearchReq() {
        return searchReq;
    }

}