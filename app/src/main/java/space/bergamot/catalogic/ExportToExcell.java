package space.bergamot.catalogic;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.SheetBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import io.realm.Realm;
import io.realm.Sort;
import space.bergamot.catalogic.recycler_add_items_groups.Dataitem;
import space.bergamot.catalogic.recycler_main.Group;

public class ExportToExcell {
    private static WeakReference<Context> contextWeakReference;
    public ExportToExcell(Context context)
    {
        contextWeakReference = new WeakReference<>(context);
    }
    static ExportToExcell exportToExcell;
    static ExportToExcell.Export exportAsynk;

    public static File getExportFile(Context context)
    {
        exportToExcell = new ExportToExcell(context);
        exportAsynk = new ExportToExcell.Export();
        try {
            return exportAsynk.execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    static class Export extends AsyncTask<Void, Void, File>
    {

        private Workbook workbook;
        private File file;
        private FileOutputStream fileOutputStream;
        private File storageDir;
        private Realm realm;

        @Override
        protected File doInBackground(Void... voids) {
            realm               = realm.getDefaultInstance();
            String timeStamp    = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String xlsFileName  = "Catalogic_export_" + timeStamp + ".xls";
            storageDir          = contextWeakReference.get().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
            file                = new File(storageDir, xlsFileName);
            workbook            = new HSSFWorkbook();
            List<Group> groups  = realm.where(Group.class).findAll();

            for (Group group : groups)
            {
                Sheet sheet = workbook.createSheet(group.getName());
                List<Dataitem> dataitems = realm
                        .where(Dataitem.class)
                        .equalTo("groups",group.getPrimKey())
                        .sort("date", Sort.DESCENDING)
                        .findAll();

                // Beginn inncer Cycle
                for (int i = 0; i < dataitems.size();i++)
                {
                    Dataitem dataitem = dataitems.get(i);
                    Row row = sheet.createRow(i);
                    row.createCell(0).setCellValue(dataitem.getName());
                    if (!TextUtils.isEmpty(dataitem.getDescription()))row.createCell(1).setCellValue(dataitem.getDescription());
                } // End Cycle for (Dataitem dataitem : dataitems)

            } // End Cycle for (Group group : groups)

            try {
                fileOutputStream = new FileOutputStream(file);
                workbook.write(fileOutputStream);
                fileOutputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
                file = null;
                return file;
            }
            realm.close();
            return file;
        }
    }
}






















