package space.bergamot.catalogic;

import android.app.Application;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import space.bergamot.catalogic.recycler_add_items_groups.Dataitem;
import space.bergamot.catalogic.recycler_main.Group;

public class InitApplication extends Application {
    @Override
    public void onCreate()
    {
        super.onCreate();
        Realm.init(this);
//        Realm.deleteRealm(Realm.getDefaultConfiguration());
        cleanDatabase();
    }

    private void cleanDatabase()
    {
        Realm realm = Realm.getDefaultInstance();
        ArrayList<File> filesToDelete = new ArrayList<>();
        ArrayList<Dataitem> dataitemsToDelete = new ArrayList<>();
        List<Group> groups = realm.where(Group.class).findAll();
        List<Dataitem> dataitems = realm.where(Dataitem.class).findAll();
        boolean toDelete;
        for (Dataitem dataitem : dataitems) {
            toDelete = true;
            for (Group group : groups) {
                if (dataitem.getGroups().contains(group.getPrimKey())) toDelete = false;
            }
            if (toDelete) {
                dataitemsToDelete.add(dataitem);
                if(!TextUtils.isEmpty(dataitem.getPhoto()))filesToDelete.add(new File(dataitem.getPhoto()));
            }
        }

        for (File file : filesToDelete) {
            File dir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File f = new File(dir,file.getName());
            file = null;
        }

        for (Dataitem dataitem : dataitemsToDelete)
            realm.executeTransaction(realm1 -> realm
                    .where(Dataitem.class)
                    .equalTo("primKey", dataitem.getPrimKey())
                    .findAll()
                    .deleteAllFromRealm());
        realm.close();
    }
}
