package space.bergamot.catalogic.recycler_add_items_groups;

import java.util.Date;
import java.util.Random;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Dataitem extends RealmObject {
    private String name;
    private String nameLowerCase;
    private String description;
    private String photo;
    private String groups;
    private String data;
    private String date;
    @PrimaryKey
    private String primKey;


    // Constr
    public Dataitem()
    {
        name = "";
        generatePk();
    }
    public Dataitem(String name, String groups)
    {
        this.name = name;
        this.nameLowerCase = name.toLowerCase();
        this.groups = groups;
        date = new Date().toString();
        generatePk();
    }


    // Getters, Setters and GeneratePk
    public String getPrimKey() {return primKey;}
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    public String getDescription() {return description;}
    public void setDescription(String description) {this.description = description;}
    public String getPhoto() {return photo;}
    public void setPhoto(String photo) {this.photo = photo;}
    public String getGroups() {return groups;}
    public void setGroups(String groups) {this.groups = groups;}
    private void generatePk(){primKey = name + String.valueOf(new Random().nextInt(100000)+"g");}
    public String getData() {return data;}
    public void setData(String data) {this.data = data;}
}
