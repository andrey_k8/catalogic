package space.bergamot.catalogic.recycler_add_items_groups;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import space.bergamot.catalogic.R;
import space.bergamot.catalogic.recycler_main.Group;

public class MyAddItemRecyclerAdapter extends RecyclerView.Adapter<MyAddItemRecyclerAdapter.MyAddItemRecyclerHolder> {

    private List<Group> list;
    private Set<Group> checked = new HashSet<>();
    private WeakReference<Context> context;
    private String lastCheckedID;


    public MyAddItemRecyclerAdapter(List<Group> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public MyAddItemRecyclerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        context = new WeakReference<>(parent.getContext());
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_add_item_grouplist_view, parent, false);
        return new MyAddItemRecyclerHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAddItemRecyclerHolder holder, int position)
    {
        holder.bind(list.get(position));
        holder.view.setOnClickListener(view -> {
            if (checked.contains(list.get(position))) checked.remove(list.get(position));
            else {
                checked.add(list.get(position));
                lastCheckedID = list.get(position).getPrimKey();
            }

            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public String getChecked()
    {
        StringBuilder s = new StringBuilder();
        for (Group group : checked)  s.append(group.getPrimKey());
        return s.toString();
    }

    public void addChecked(Group group){checked.add(group);}


    public void setChecked(String groups)
    {
        for (String groupKey:groups.split("g")) {
            for (Group group:list) if (group.getPrimKey().equals(groupKey+"g")) checked.add(group);
        }
    }







    class MyAddItemRecyclerHolder extends RecyclerView.ViewHolder {

        View view;
        TextView groupName;
        ImageView color;
        ImageView colorTextBackground;


        MyAddItemRecyclerHolder(@NonNull View itemView)
        {
            super(itemView);
            view = itemView;
            colorTextBackground = itemView.findViewById(R.id.content_add_item__groups_list_card__text_background);
            groupName = itemView.findViewById(R.id.content_add_item__groups_list_card__text);
            color = itemView.findViewById(R.id.content_add_item__groups_list_card__image_color);
        }

        void bind(Group group)
        {
            groupName.setText(group.getName());

            if (checked.contains(group)) {
                if (lastCheckedID != null && lastCheckedID.equals(group.getPrimKey())) {
                    colorTextBackground.startAnimation(AnimationUtils.loadAnimation(context.get(),R.anim.group_list_selection_anim));
                    lastCheckedID = null;
                }
                colorTextBackground.setBackgroundColor(Color.parseColor(group.getColor()));
                color.setBackgroundColor(Color.WHITE);
            } else {
                colorTextBackground.setBackgroundColor(Color.WHITE);
                color.setBackgroundColor(Color.parseColor(group.getColor()));
            }
        }
    }
}
