package space.bergamot.catalogic;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.exifinterface.media.ExifInterface;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import io.realm.Realm;
import space.bergamot.catalogic.recycler_add_items_groups.Dataitem;
import space.bergamot.catalogic.recycler_add_items_groups.MyAddItemRecyclerAdapter;
import space.bergamot.catalogic.recycler_main.Group;

public class AddItemActivity extends AppCompatActivity {

	RecyclerView recyclerView;
	MyAddItemRecyclerAdapter adapter;
	Realm realm;
	private TextInputEditText inputEditText;
	private TextInputEditText inputDescEditText;
	private ImageView imageView;
	private String imagePath;
	private File photoFile;
	private File newSmallImage;
	private ExifInterface ei;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_item);

//
//        this.getWindow()
//                .setSoftInputMode(WindowManager
//                        .LayoutParams
//                        .SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		realm = Realm.getDefaultInstance();
		inputEditText = findViewById(R.id.content_add_item___edit_text);
		inputDescEditText = findViewById(R.id.content_add_item__description_input_edit_text);
		imageView = findViewById(R.id.imageView);
		imageView.setOnClickListener(view -> selectImage());

		recyclerView = findViewById(R.id.content_add_item__groups_list);
		{
			adapter = new MyAddItemRecyclerAdapter(realm.where(Group.class).findAll());
			recyclerView.setLayoutManager(new LinearLayoutManager(this));
			recyclerView.setAdapter(adapter);
			if (getIntent().hasExtra("groupID"))
				adapter.addChecked(realm
						.where(Group.class)
						.equalTo("primKey", getIntent().getStringExtra("groupID"))
						.findFirst());
		}

		Toolbar toolbar = findViewById(R.id.toolbar);
		{
			setSupportActionBar(toolbar);
			Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
			toolbar.setNavigationOnClickListener(v -> onBackPressed());
			findViewById(R.id.appBarLayout_add_item).bringToFront();
		}

		FloatingActionButton fab = findViewById(R.id.fab);
		{
			fab.setOnClickListener(view -> {
				String name = String.valueOf(inputEditText.getText()).trim();
				String groups = adapter.getChecked();

				if (TextUtils.isEmpty(name)) {
					inputEditText.setError(getString(R.string.error_no_name));
				} else if (TextUtils.isEmpty(groups)) {
					Snackbar.make(view, getString(R.string.error_no_group), Snackbar.LENGTH_SHORT)
							.setAction("Action", null).show();
				} else {

					if (!TextUtils.isEmpty(getIntent().getStringExtra("itemID"))) {
						updateItemInDb();
					} else {
						Dataitem dataitem = new Dataitem(name, groups);
						dataitem.setPhoto(imagePath);
						dataitem.setDescription(String.valueOf(inputDescEditText.getText()).trim());
						addItemToDb(dataitem);
					}
				}
			});
		}

		// If it starts with data
		if (!TextUtils.isEmpty(getIntent().getStringExtra("itemID"))) {
			Dataitem dataitem = realm.where(Dataitem.class)
					.equalTo("primKey", getIntent().getStringExtra("itemID"))
					.findFirst();
			inputEditText.setText(dataitem.getName());
			inputDescEditText.setText(dataitem.getDescription());
			if (dataitem.getPhoto() != null) {
				imagePath = dataitem.getPhoto();
				Picasso.get()
						.load(imagePath)
						.into(imageView);
			}
			adapter.setChecked(dataitem.getGroups());

			setTitle(dataitem.getName());
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		realm.close();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode !=
				RESULT_CANCELED) {
			imageView.setVisibility(View.INVISIBLE);
			findViewById(R.id.prog_bar_photo).setVisibility(View.VISIBLE);
			switch (requestCode) {
				case 5345:
					if (resultCode == RESULT_OK && data != null) {
						Uri selectedImage = data.getData();
						try {
							photoFile = createImageFile();
							ei = new ExifInterface(selectedImage.getEncodedPath());
						} catch (IOException e) {
							e.printStackTrace();
						}
						Uri uri = Uri.fromFile(photoFile);
						Crop.of(selectedImage, uri).asSquare().start(this);
					}
					break;
				case 7613:
					if (resultCode == RESULT_OK) {
						Uri uri = Uri.fromFile(photoFile);
						try {
							ei = new ExifInterface(uri.getEncodedPath());
						} catch (IOException e) {
							e.printStackTrace();
						}
						Crop.of(uri, uri).asSquare().start(this);
					}
					break;
				case Crop.REQUEST_CROP:
					if (resultCode == RESULT_OK) {
						new AsyncTask<File, Void, File>() {
							@Override
							protected File doInBackground(File... files) {
								newSmallImage = createSmallImage(photoFile);
								imagePath = newSmallImage.toURI().toString();
								return null;
							}

							@Override
							protected void onPostExecute(File file) {
								if (imagePath != null) {
									Picasso.get()
											.load(imagePath)
											.into(imageView);
								}
								photoFile.delete();
							}
						}.execute(photoFile);
					}
					break;
			}
			findViewById(R.id.prog_bar_photo).setVisibility(View.INVISIBLE);
			imageView.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		if (!TextUtils.isEmpty(getIntent().getStringExtra("itemID"))) {
			Dataitem dataitem = realm.where(Dataitem.class)
					.equalTo("primKey", getIntent().getStringExtra("itemID"))
					.findFirst();
			if (dataitem.getPhoto() != null && !imagePath.equals(dataitem.getPhoto()) && imagePath != null) {
				File dir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
				new File(dir, new File(imagePath).getName()).delete();
			}
		} else {
			if (imagePath != null) {
				File dir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
				new File(dir, new File(imagePath).getName()).delete();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!TextUtils.isEmpty(getIntent().getStringExtra("itemID")))
			getMenuInflater().inflate(R.menu.menu_add_item, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		int id = item.getItemId();

		if (id == R.id.action_delete) {
			if (!TextUtils.isEmpty(imagePath)) {
				File dir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
				new File(dir, new File(imagePath).getName()).delete();
			}
			realm.executeTransaction(realm1 -> realm
					.where(Dataitem.class)
					.equalTo("primKey", getIntent().getStringExtra("itemID"))
					.findAll()
					.deleteAllFromRealm());
			finish();
		}

		return super.onOptionsItemSelected(item);
	}


	private void addItemToDb(Dataitem dataitem) {
		realm.executeTransaction(realm -> realm.insertOrUpdate(dataitem));
		finish();
	}

	private void updateItemInDb() {
		Dataitem dataitemToUpdate = realm.where(Dataitem.class)
				.equalTo("primKey", getIntent().getStringExtra("itemID"))
				.findFirst();
		realm.executeTransaction(realm -> realm
				.where(Dataitem.class)
				.equalTo("primKey", dataitemToUpdate.getPrimKey())
				.findFirst()
				.setName(String.valueOf(inputEditText.getText()).trim()));
		realm.executeTransaction(realm -> realm
				.where(Dataitem.class)
				.equalTo("primKey", dataitemToUpdate.getPrimKey())
				.findFirst()
				.setDescription(String.valueOf(inputDescEditText.getText()).trim()));
		realm.executeTransaction(realm -> realm
				.where(Dataitem.class)
				.equalTo("primKey", dataitemToUpdate.getPrimKey())
				.findFirst()
				.setPhoto(imagePath));
		String groups = adapter.getChecked();
		realm.executeTransaction(realm -> realm
				.where(Dataitem.class)
				.equalTo("primKey", dataitemToUpdate.getPrimKey())
				.findFirst()
				.setGroups(groups));
		finish();
	}

	private void selectImage() {
		final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.chose_from_gallery)};
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.title_choose_pic));
		builder.setItems(options, (dialog, item) -> {
			if (options[item].equals(getString(R.string.take_photo))) {
				Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				// Ensure that there's a camera activity to handle the intent
				if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
					// Create the File where the photo should go
					photoFile = null;
					try {
						photoFile = createImageFile();
					} catch (IOException ex) {
						// Error occurred while creating the File
					}
					// Continue only if the File was successfully created
					if (photoFile != null) {
						Uri photoURI = FileProvider.getUriForFile(this,
								"space.bergamot.android.fileprovider",
								photoFile);
						imagePath = photoURI.toString();
						takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
						startActivityForResult(takePictureIntent, 7613);
					}
				}
			} else if (options[item].equals(getString(R.string.chose_from_gallery))) {
				Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
				photoPickerIntent.setType("image/*");
				if (photoPickerIntent.resolveActivity(getPackageManager()) != null)
					startActivityForResult(photoPickerIntent, 5345);
			}
		});
		builder.show();
	}

	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
		File image = File.createTempFile(
				imageFileName,  /* prefix */
				".jpg",         /* suffix */
				storageDir      /* directory */
		);


		return image;
	}

	private File createSmallImage(File file) {
		File smallImage = null;
		try {
			smallImage = createImageFile();
			Bitmap original = BitmapFactory.decodeFile(file.getPath());
			int q = 0;
			if (original.getWidth() > 4000) q = 4;
			else if (original.getWidth() > 2000) q = 2;
			else q = 1;
			int width = original.getWidth() / q;
			int height = original.getHeight() / q;
			Bitmap small = Bitmap.createScaledBitmap(original, width, height, false);
			FileOutputStream fos = new FileOutputStream(smallImage);

			int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_UNDEFINED);

			switch (orientation) {
				case ExifInterface.ORIENTATION_ROTATE_90:
                    small = rotateImage(small, 90);
					break;

				case ExifInterface.ORIENTATION_ROTATE_180:
                    small = rotateImage(small, 180);
					break;

				case ExifInterface.ORIENTATION_ROTATE_270:
                    small = rotateImage(small, 270);
					break;

				case ExifInterface.ORIENTATION_NORMAL:
				default:
			}
			small.compress(Bitmap.CompressFormat.JPEG, 80, fos);
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return smallImage;
	}


	public static Bitmap rotateImage(Bitmap source, float angle) {
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
				matrix, true);
	}
}