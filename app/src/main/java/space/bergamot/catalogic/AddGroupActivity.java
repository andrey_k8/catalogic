package space.bergamot.catalogic;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;

import io.realm.Realm;
import space.bergamot.catalogic.recycler_main.Group;

public class AddGroupActivity extends AppCompatActivity implements View.OnClickListener {


    Realm realm;

    private MaterialButton mb1;
    private MaterialButton mb2;
    private MaterialButton mb3;
    private MaterialButton mb4;
    private MaterialButton mb5;
    private MaterialButton mb6;
    private MaterialButton mb7;
    private MaterialButton mb8;
    private MaterialButton mb9;
    private MaterialButton mb10;
    private MaterialButton mb11;
    private MaterialButton mb12;
    private ArrayList<MaterialButton> materialButtonsList;
    private TextInputEditText inputEditText;
    private String color = "#CD999999";
    private Group group;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group);
        setTitle(R.string.title_activity_add_group);
        realm = Realm.getDefaultInstance();
        inputEditText = findViewById(R.id.content_add_group__text);

        Toolbar toolbar = findViewById(R.id.toolbar);
        {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }


        {
            mb1 = findViewById(R.id.color_picker__color1);
            mb2 = findViewById(R.id.color_picker__color2);
            mb3 = findViewById(R.id.color_picker__color3);
            mb4 = findViewById(R.id.color_picker__color4);
            mb5 = findViewById(R.id.color_picker__color5);
            mb6 = findViewById(R.id.color_picker__color6);
            mb7 = findViewById(R.id.color_picker__color7);
            mb8 = findViewById(R.id.color_picker__color8);
            mb9 = findViewById(R.id.color_picker__color9);
            mb10 = findViewById(R.id.color_picker__color10);
            mb11 = findViewById(R.id.color_picker__color11);
            mb12 = findViewById(R.id.color_picker__color12);

            materialButtonsList = new ArrayList<>();
            materialButtonsList.add(mb1);
            materialButtonsList.add(mb2);
            materialButtonsList.add(mb3);
            materialButtonsList.add(mb4);
            materialButtonsList.add(mb5);
            materialButtonsList.add(mb6);
            materialButtonsList.add(mb7);
            materialButtonsList.add(mb8);
            materialButtonsList.add(mb9);
            materialButtonsList.add(mb10);
            materialButtonsList.add(mb11);
            materialButtonsList.add(mb12);
            for (MaterialButton button : materialButtonsList) button.setOnClickListener(this);
        }


        GridLayout gridLayout = findViewById(R.id.color_picker_grid_layout);
        {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int width = displayMetrics.widthPixels;
            gridLayout.setColumnCount(width / 180);
        }


        if (!TextUtils.isEmpty(getIntent().getStringExtra("primKey")))
        {
            group = realm
                    .where(Group.class)
                    .equalTo("primKey", getIntent().getStringExtra("primKey"))
                    .findFirst();
            color = group.getColor();
            setTitle(group.getName());
            inputEditText.setText(group.getName());
            for (MaterialButton button : materialButtonsList)
                if (button.getTag().equals(group.getColor()))
                    button.setIconResource(R.drawable.ic_submit);
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        {
            fab.setOnClickListener(view -> {
                        String name = String.valueOf(inputEditText.getText()).trim();
                        if (TextUtils.isEmpty(name)) {
                            inputEditText.setError(getString(R.string.error_no_name));
                        } else if (group != null) {
                            realm.executeTransaction(realm -> realm
                                    .where(Group.class)
                                    .equalTo("primKey", group.getPrimKey())
                                    .findFirst()
                                    .setColor(color));
                            realm.executeTransaction(realm -> realm
                                    .where(Group.class)
                                    .equalTo("primKey", group.getPrimKey())
                                    .findFirst()
                                    .setName(name));
                            finish();
                        } else {
                            realm.executeTransaction(realm -> realm.insertOrUpdate(new Group(name, color)));
                            finish();

                        }

                    }
            );
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (!TextUtils.isEmpty(getIntent().getStringExtra("primKey"))) {
            getMenuInflater().inflate(R.menu.menu_add_group, menu);
            return true;
        } else return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_delete) {
            realm.executeTransaction(realm1 -> realm
                    .where(Group.class)
                    .equalTo("primKey", getIntent().getStringExtra("primKey"))
                    .findAll()
                    .deleteAllFromRealm());
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view)
    {
        if (view instanceof MaterialButton) {
            for (MaterialButton button : materialButtonsList) button.setIcon(null);
            ((MaterialButton) view).setIconResource(R.drawable.ic_submit);
            color = (String) view.getTag();
        }

    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        realm.close();
    }
}
