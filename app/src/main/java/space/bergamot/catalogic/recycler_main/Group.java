package space.bergamot.catalogic.recycler_main;

import java.util.Random;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Group extends RealmObject {
    private String name;
    private String color;
    @PrimaryKey
    private String primKey;

    // Constr
    public Group()
    {
        name = "";
        generatePk();
    }

    public Group(String name, String color)
    {
        this.name = name;
        this.color = color;
        generatePk();
    }

    // Getter Setter Generate
    public String getPrimKey() {        return primKey;    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }

    private void generatePk(){
        primKey = name + String.valueOf(new Random().nextInt(100000)+"g");
    }
}
