package space.bergamot.catalogic.recycler_main;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.BackgroundColorSpan;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import java.util.List;

public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    private List<Group> groups;
    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public SectionsPagerAdapter(FragmentManager fm, List<Group> groups)
    {
        super(fm);
        this.groups = groups;
    }

    @Override
    public int getItemPosition(@NonNull Object object)
    {
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public Fragment getItem(int position)
    {
        return PlaceholderFragment.newInstance(groups.get(position).getPrimKey());
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position)
    {
        Group group = groups.get(position);
        SpannableString sb = new SpannableString(" "+ group.getName() +" ");
        sb.setSpan(
                new BackgroundColorSpan(Color.parseColor(group.getColor())),
                0,sb.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE

        );
        sb.setSpan(
                new AbsoluteSizeSpan(20,true),
                0,sb.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );
        return sb;
    }

    @Override
    public int getCount() {
        return groups.size();
    }

}