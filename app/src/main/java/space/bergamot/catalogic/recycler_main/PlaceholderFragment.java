package space.bergamot.catalogic.recycler_main;

import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.realm.Sort;
import space.bergamot.catalogic.MainActivity;
import space.bergamot.catalogic.R;
import space.bergamot.catalogic.recycler_add_items_groups.Dataitem;


public class PlaceholderFragment extends Fragment {

    private static final String GROUP_ID = "GROUP_ID";
    private String groupID = "";
    private RecyclerView recyclerView;
    private ItemsRecyclerAdapter itemsRecyclerAdapter;
    private List<Dataitem> dataitems;

    static PlaceholderFragment newInstance(String groupID) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putString(GROUP_ID, groupID);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            groupID = getArguments().getString(GROUP_ID);
        }


    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_group_view, container, false);
        MainActivity mainActivity = (MainActivity) getActivity();
        if (!TextUtils.isEmpty(mainActivity.getSearchReq())) {
            dataitems = mainActivity.getRealm().where(Dataitem.class)
                    .contains("groups", groupID)
                    .contains("nameLowerCase", mainActivity.getSearchReq().toLowerCase())
                    .sort("date", Sort.DESCENDING)
                    .findAll();
        } else {
            dataitems = mainActivity.getRealm().where(Dataitem.class)
                    .contains("groups",groupID)
                    .sort("date", Sort.DESCENDING)
                    .findAll();
        }
        recyclerView = view.findViewById(R.id.fragment_item_list___recycler);
        int orientation = this.getResources().getConfiguration().orientation;
        int width;
        width = (orientation == Configuration.ORIENTATION_PORTRAIT)? 2 : 4;
        recyclerView.setLayoutManager(new GridLayoutManager(mainActivity, width));
        itemsRecyclerAdapter = new ItemsRecyclerAdapter(dataitems);
        recyclerView.setAdapter(itemsRecyclerAdapter);
        return view;
    }

}